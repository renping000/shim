Anolis OS
=======================================
# 代码仓库说明
## 分支说明
>进行代码开发工作时，请注意选择当前版本对应的分支
* aX分支为对应大版本的主分支,如a8分支对应当前最新版本
* aX.Y分支为对应小版本的维护分支，如a8.2分支对应8.2版本
## 开发流程
1. 首先fork目标分支到自己的namespace
2. 在自己的fork分支上做出修改
3. 向对应的仓库中提交merge request，源分支为fork分支
